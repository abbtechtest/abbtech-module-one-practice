package abbtech.lesson8;

public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("Kitab1", "Müəllif1", "Janr1", 1990);
        Book book2 = new Book("Kitab2", "Müəllif2", "Janr2", 1995);
        Book book3 = new Book("Kitab3", "Müəllif1", "Janr1", 2000);

        Library library = new Library();
        library.addBook(book1);
        library.addBook(book2);
        library.addBook(book3);

        System.out.println("Bütün Kitablar:");
        library.displayAllBooks();

        System.out.println("\nBaşlığa görə axtarış: 'Kitab1'");
        library.displayBooks(library.searchByTitle("Kitab1"));

        System.out.println("\nMüəllifə görə axtarış: 'Müəllif1'");
        library.displayBooks(library.searchByAuthor("Müəllif1"));

        System.out.println("\nJanra görə axtarış: 'Janr1'");
        library.displayBooks(library.searchByGenre("Janr1"));

        System.out.println("\nBaşlığa görə sıraya salmaq:");
        library.displayBooks(library.sortByTitle());

        System.out.println("\nMüəllifə görə sıraya salmaq:");
        library.displayBooks(library.sortByAuthor());

        System.out.println("\nNəşr ilinə görə sıraya salmaq:");
        library.displayBooks(library.sortByPublicationYear());

        System.out.println("\nOrta nəşr ili: " + library.averagePublicationYear());

        System.out.println("\nKitabların sayı: " + library.countBooks());

        library.removeBook(book2);
        System.out.println("\n'Kitab2' kitabı silindikdən sonra bütün kitablar:");
        library.displayAllBooks();
    }
}