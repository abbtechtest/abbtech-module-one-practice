package abbtech.lesson12;

public class Main {
    public static void main(String[] args) {
        Calculator c = new Calculator();
        double a = 30.0;
        double b = 5;

        System.out.println(a + " və " + b + " ədədlərinin cəmi " + c.add(a, b) + " edir");
        System.out.println(a + " və " + b + " ədədlərinin hasilatı " + c.multiply(a, b) + " edir");
        System.out.println(a + " ədədindən " + b + " ədədini çıxmaq nəticəsi " + c.subtract(a, b) + " edir");
        System.out.println(a + " ədədini " + b + " ədədinə bölən nəticə " + c.divide(a, b) + " edir");
    }
}
