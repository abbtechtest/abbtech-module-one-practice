package abbtech.lesson11.task;

public class LiveLockExample {
    static class Process {
        private boolean hasResource = false;

        public void doWork(Process partner) {
            while (!hasResource) {
                System.out.println(Thread.currentThread().getName() + " : Mən resursu gözləyirəm.");

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (!partner.hasResource) {
                    System.out.println(Thread.currentThread().getName() + " : yeniden cehd edin");
                    continue;
                }

                System.out.println(Thread.currentThread().getName() + " : Mən sənə resursu verəcəm.");
                hasResource = true;
            }
        }
    }

    public static void main(String[] args) {
        final Process p1 = new Process();
        final Process p2 = new Process();

        Thread t1 = new Thread(() -> p1.doWork(p2), "Thread-1");
        Thread t2 = new Thread(() -> p2.doWork(p1), "Thread-2");

        t1.start();
        t2.start();
    }
}
