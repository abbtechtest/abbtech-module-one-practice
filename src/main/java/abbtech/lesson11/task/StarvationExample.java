package abbtech.lesson11.task;
import java.util.concurrent.locks.ReentrantLock;
public class StarvationExample {
    static class Resource {
        private final ReentrantLock lock = new ReentrantLock();

        public void accessResource(String threadName) {
            lock.lock();

            try {
                System.out.println(threadName + " : Resursa daxil olur.");
                Thread.sleep(1000); // İşi təmsil et
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    public static void main(String[] args) {
        final Resource resource = new Resource();

        Thread t1 = new Thread(() -> resource.accessResource("Thread-1"), "Thread-1");
        Thread t2 = new Thread(() -> resource.accessResource("Thread-2"), "Thread-2");
        Thread t3 = new Thread(() -> resource.accessResource("Thread-3"), "Thread-3");

        t1.start();
        t2.start();
        t3.start();
    }
}
