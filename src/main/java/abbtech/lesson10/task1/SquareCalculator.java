package abbtech.lesson10.task1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SquareCalculator {
    public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    boolean validInput = false;

    while (!validInput) {
        try {
            System.out.print("Bir tam ədəd daxil edin: ");
            int num = scanner.nextInt();
            int square = num * num;
            System.out.println(num + " ədədinin kvadratı: " + square);
            validInput = true;
        } catch (InputMismatchException e) {
            System.out.println("Yanlış daxil edildi. Zəhmət olmasa düzgün bir tam ədəd daxil edin.");
            scanner.nextLine();
        }
    }

    scanner.close();
}
}