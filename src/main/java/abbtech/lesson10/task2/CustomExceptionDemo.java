package abbtech.lesson10.task2;

import java.util.Scanner;

public class CustomExceptionDemo {
    public static void validateTemperature(int temperature) throws InvalidInputException {
        final int MIN_TEMP = -50;
        final int MAX_TEMP = 50;

        if (temperature < MIN_TEMP || temperature > MAX_TEMP) {
            throw new InvalidInputException("Temperatur -50 ilə 50 arasında olmalıdır.");
        }

        System.out.println("Düzgün temperatur daxil edildi: " + temperature);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.print("Temperatur daxil edin: ");
            int temperature = scanner.nextInt();
            validateTemperature(temperature);
        } catch (InvalidInputException e) {
            System.out.println("Yanlış daxil edildi: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Yanlış daxil edildi: Zəhmət olmasa düzgün bir tam ədəd daxil edin.");
            scanner.nextLine();
        } finally {
            scanner.close();
        }
    }
}