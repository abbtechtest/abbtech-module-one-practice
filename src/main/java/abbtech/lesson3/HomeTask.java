package abbtech.lesson3;
public class HomeTask {
    public static void main(String[] args) {
        System.out.println("Task1");
        Task1();
        System.out.println("Task2");
        Task2();
        System.out.println("Task3");
        String result = removeDublicate("w3resource");
        System.out.println(result);
        System.out.println("Task5");
        Task5();
        System.out.println("Task6");
        Task6();
        System.out.println("Task7");
        Task7();
        System.out.println("Task9");
        Task9();
        System.out.println("Task10");
        Task10();
    }
    public static void Task1() {
        for (char simvol = 'A'; simvol <= 'Z'; simvol++) {
            int indeks = simvol - 'A' + 1;
            System.out.println(simvol + " - " + indeks);
        }
    }

    public static void Task2() {
        String s = "The quick brown fox jumps over the lazy dog.";
        System.out.println(s.replaceAll("fox", "cat"));
    }

    public static String removeDublicate(String s) {
        StringBuilder resultBuilder = new StringBuilder();
        boolean[] seen = new boolean[256];
        for (char c : s.toCharArray()) {
            if (!seen[c]) {
                resultBuilder.append(c);
                seen[c] = true;
            }
        }
        return resultBuilder.toString();
    }

    private static void Task5() {

        String str = "abcdefghijklmnopqrstuvwxy";
        int n = 5;
        System.out.println("Sözün bərabər hissələri: ");
        int partLength = str.length() / n;

        for (int i = 0; i < n; ++i) {
            int startIndex = i * partLength;
            int endIndex = (i + 1) * partLength;
            if (i == n - 1) {
                endIndex = str.length();
            }

            String part = str.substring(startIndex, endIndex);
            System.out.println("Hissə " + (i + 1) + ": " + part);
        }

    }

    public static void Task6() {
        String input = "Reverse words in a given string";
        String[] words = input.split("\\s+");
        String reversedString = "";

        for (int i = words.length - 1; i >= 0; --i) {
            reversedString = reversedString + words[i];
            if (i > 0) {
                reversedString = reversedString + " ";
            }
        }

        System.out.print("Sözlər tərsinə çevrilmişdir:");
        System.out.println(reversedString);
    }

    public static void Task7() {

        String input = "ab5c2d4ef12s";
        int sum = 0;

        for (int i = 0; i < input.length(); ++i) {
            char ch = input.charAt(i);
            if (Character.isDigit(ch)) {
                int digit = Character.getNumericValue(ch);
                sum += digit;
            }
        }

        System.out.println("Sətirdəki rəqəmlərin cəmi: " + sum);

    }

    public static void Task9() {
        int[] array = {3, 5, 7, 2, 8, -1, 4, 10, 12};

        int max = array[0];
        int min = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
            if (array[i] < min) {
                min = array[i];
            }
        }

        System.out.println("massivin en boyuk elementi: " + max);
        System.out.println("massivin en kicik elementi: " + min);
    }

    public static void Task10() {
        int[] array = {3, 5, 7, 2, 8, 3, 3, 4, 10, 3, 12};

        int valueToCount = 3;
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == valueToCount) {
                count++;
            }
        }

        System.out.println("value " + valueToCount + " massiv " + count + " neçə dəfə təkrarlanır");
    }
}


